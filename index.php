<?php

   require_once(dirname(__FILE__) . '\php\dbConfig.php');
   require_once(dirname(__FILE__) . '\php\getUsers.php');

   $users = getUsers($conn);

   if(isset($_GET['searchUser'])) {
      $qry_string = $_GET['query'];
      
      $sql = "SELECT * FROM users WHERE username LIKE '%{$qry_string}%'";
      $stmt = $conn->query($sql);
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      echo $result;
      $users = $result;
      header("Location: http://localhost/testOne/index.php");
  }

?>

   <?php include_once('./includes/header.php'); ?>

   <div class="container">
      <div class="row">
         <div class="col-md-8 mx-auto">
            <form class="m-4" method="post" action="./php/createUser.php">
               <div class="form-group">
                     <input 
                        type="text"
                        name="username"
                        class="form-control"
                        placeholder="username">
               </div>
               <div class="form-group">
                     <input type="email" name="email" class="form-control" placeholder="email">
               </div>
               <div class="form-group">
                     <input type="address" name="address" class="form-control" placeholder="address">
               </div>

               <button type="submit" name="julsubmit" class="d-flex align-items-center btn btn-primary">
               <i class="material-icons mr-2">add</i>Create User</button>
            </form>
         </div>

         <div class="col-12 d-flex justify-content-end">
            <form class="w-30" method="get" action="./index.php">
               <div class="input-group mb-3">
                  <input type="text" class="form-control" name="query" placeholder="search username">
                  <div class="input-group-append">
                     <button
                        type="submit"
                        name="searchUser"
                        class="btn btn-outline-primary">Search</button>
                  </div>
               </div>
            </form>
         </div>

         <div class="col-md-12">
            <table class="table">
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">username</th>
                  <th scope="col">email</th>
                  <th scope="col">address</th>
                  <th scope="col">actions</th>
               </tr>
            </thead>
            <tbody>
               <?php foreach($users as $user) : ?>
                  <tr>
                     <th scope="row"><?php echo $user['id'] ?></th>
                     <td><?php echo $user['username'] ?></td>
                     <td><?php echo $user['email'] ?></td>
                     <td><?php echo $user['address'] ?></td>
                     <td>
                        <a
                           href="./php/editUser.php/?user_id=<?php echo $user['id'] ?>"
                           class="btn btn-sm btn-warning"><i class="material-icons">edit</i>
                        </a>
                        <button
                           onclick="deleteUser(<?php echo $user['id'] ?>)"
                           class="btn btn-sm btn-danger"><i class="material-icons">delete</i>
                        </button>
                     </td>
                  </tr>
               <?php endforeach ?>
            </tbody>
            </table>
         </div>
      </div>
   </div>

   <script>
      function deleteUser(user_id) {
         confirm('are you sure you want to delete');
         location.href = `http://localhost/testOne/php/deleteUser.php/?user_id=${user_id}`
      };
   </script>
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    
</body>
</html>