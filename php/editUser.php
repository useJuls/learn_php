<?php

    require_once(dirname(__FILE__) . '\dbConfig.php');

    $user_id = $_GET['user_id'];
    $sql = "SELECT * FROM users WHERE id = {$user_id}";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    if(isset($_POST['editUser'])) {
        $user_id = $_GET['user_id'];
        $username = $_POST['username'];
        $email = $_POST['email'];
        $address = $_POST['address'];

        $sql = "UPDATE users SET username='${username}', email='{$email}',address='{$address}' WHERE id={$user_id}";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        header("Location: http://localhost/testOne/index.php");
    }

?>

<?php include_once('../includes/header.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div class="container d-flex align-items-center justify-content-center" style="height: 90vh;">
        <div class="col-8 mx-auto">
            <a href="http://localhost/testOne/index.php">Go back</a>
            <h3 class="text-secondary text-center">Edit <?php echo $data['username'] ?></h3>

            <form 
                class="my-4" 
                method="post"
                action="editUser.php?user_id=<?php echo $data['id'] ?>">
                
               <div class="form-group">
                     <input 
                        value="<?php echo $data['username'] ?>"
                        type="text"
                        name="username"
                        class="form-control"
                        placeholder="username">
               </div>
               <div class="form-group">
                     <input
                     value="<?php echo $data['email'] ?>"
                        type="email" 
                        name="email" 
                        class="form-control" 
                        placeholder="email">
               </div>
               <div class="form-group">
                     <input
                        value="<?php echo $data['address'] ?>"
                        type="address"
                        name="address"
                        class="form-control" 
                        placeholder="address">
               </div>

               <button type="submit" name="editUser" class="d-flex align-items-center btn btn-warning">
               <i class="material-icons mr-2">edit</i>Edit User</button>
            </form>
        </div>
    </div>
</body>
</html>